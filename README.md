# README #

Docker container for SophiaNetTest

### What is this repository for? ###

* SophiaNetTest
* 1.0

### Docker repository ###

* konstantinoskousias/sophianettest

### Parameters ###

- "cnf_server_host":"128.39.37.146","cnf_server_port":10080 (These parameters are now hardcoded and no longer needed)
- Duration: 300 seconds

### Who do I talk to? ###

Konstantinos Kousias - kostas@simula.no